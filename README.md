# tempREST

This is a simple webserver that returns the current temperature and
humidity reading a DHT22 sensor.

The DHT library is a submodule, it can be pulled with 
```bash
git submodule
update --init
```

Another dependency is the orange pi gpio library, it can be installed
with
```bash
cd
git clone https://github.com/duxingkei33/orangepi_PC_gpio_pyH3
cd orangepi_PC_gpio_pyH3
sudo python setup.py install
```

For more informations about the DHT library see

* https://www.piprojects.xyz/temperature-sensor-orange-pi-python-code/
* https://github.com/jingl3s/DHT11-DHT22-Python-library-Orange-PI


The web server is written in Flask, so it must be installed (via `pip`
or via `apt`).
