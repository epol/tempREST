#!/usr/bin/env python3

import flask

import pyA20.gpio
import lib.dht.dht as dht
import time

# REST app
app = flask.Flask(__name__)

# Sensor
PIN = pyA20.gpio.port.PA6
pyA20.gpio.gpio.init()
sensor = dht.DHT(pin=PIN)

def get_reading():
    for i in range(10):
        result = sensor.read()
        if result.is_valid():
            return (result.temperature,result.humidity)
        time.sleep(0.5)
    return None

@app.route('/v0/ambient', methods=['GET'])
def get_ambient():
    result = get_reading()
    if result:
        return flask.jsonify({'temperature': result[0], 'humidity':result[1]})
    else:
        return flask.made_response(flask.jsonify({'error':'Too invalid data received from sensor'},503))

@app.route('/v0/temperature', methods=['GET'])
def get_temperature():
    result = get_reading()
    if result:
        return flask.jsonify({'temperature': result[0]})
    else:
        return flask.made_response(flask.jsonify({'error':'Too invalid data received from sensor'},503))
    return flask.jsonify({'temperature': 100})

@app.route('/v0/humidity', methods=['GET'])
def get_humidity():
    result = get_reading()
    if result:
        return flask.jsonify({'humidity':result[1]})
    else:
        return flask.made_response(flask.jsonify({'error':'Too invalid data received from sensor'},503))

@app.errorhandler(404)
def not_found(error):
    return flask.make_response(flask.jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(debug=True)
